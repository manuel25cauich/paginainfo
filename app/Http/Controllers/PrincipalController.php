<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrincipalController extends Controller
{
    public function inicio(){
      return view('pay.index');
    }

    public function plataforma(){
      return view('pay.plataforma');
    }

    public function beneficios(){
      return view('pay.beneficios');
    }

    public function proceso(){
      return view('pay.proceso');
    }

    public function videos(){
      return view('pay.videos');
    }

    public function contacto(){
      return view('pay.contacto');
    }

    public function regisForm(Request $req){
      $nombre = $req->nombre." ".$req->apPaterno." ".$req->apMaterno;
      return redirect('/sendSms/'.$nombre.'/'.$req->telefono.'/'.$req->correo);
    }

    public function recargas(){
      return view('pay.recargas');
    }

    public function privacite(){
      return view('pay.privacidad');
    }
}
