<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-3">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2 text-center">PayApp</h2>
                    <p>Con nuestra plataforma solo requieres un dispositivo con Whatsapp para comenzar a ganar mas dinero, compatible con todas las versiones</p>
                </div>
            </div>

            <div class="col-md-3">
                <div class="ftco-footer-widget mb-4 text-center">
                    <h2 class="ftco-heading-2">Navegación</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('inicio')}}">Inicio</a></li>
                        <li><a href="{{ route('plataforma')}}">Nuestros Servicios</a></li>
                        <li><a href="{{ route('beneficios')}}">Beneficios</a></li>
                        <li><a href="{{ route('proceso')}}">Conoce el Proceso de Venta</a></li>
                        {{--<li><a href="{{ route('videos')}}" class="py-2 d-block">Videos</a></li>--}}
                        <li><a href="{{ route('contacto')}}">Contáctanos</a></li>
                        <li><a href="https://payapp.mx/payapp/login">Acceso a Clientes</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
              <div class="ftco-footer-widget mb-4 text-center">
                <h2 class="ftco-heading-2">Redes Sociales</h2>
                <div class="block-23 mb-3 text-center">
                  <ul style="padding-left: 31%;">
                    <li><span><i class="fa fa-facebook-square fa-1x" aria-hidden="true" style="color: #3b5998"></i></span><span class="text" style="padding-left: 10%;"><a href="https://www.facebook.com/servpayapp/">Facebook</a></span></li>
                    <li><span><i class="fa fa-instagram fa-1x" aria-hidden="true" style="background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%); color: #fff;"></i></span><span class="text" style="padding-left: 10%;"><a href="https://instagram.com/payappmx?igshid=jpu2fkn1l8o2">Instragram</a></span></li>
                    <li><span><i class="fa fa-youtube-play fa-1x" aria-hidden="true" style="color: #c4302b"></i></span><span class="text" style="padding-left: 10%;"><a href="https://www.youtube.com/channel/UC_t5ZZuoeS9LOIy6FNnzkLA">YouTube</a></span></li>
                  </ul>

                </div>
              </div>

            </div>

            <div class="col-md-3">
                <div class="ftco-footer-widget mb-4">
                  <h2 class="ftco-heading-2 text-center">Contáctanos</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Periférico Sur 4194 Piso 1, <br>Col. Jardines del Pedregal<br> C.P. 01900, México, D.F.</span></li>
                            <li><a href="tel:5539294202"><span class="icon icon-phone"></span><span class="text">5539294202</span></a></li>
                            <li><a href="mailto:contacto@payapp.com"><span class="icon icon-envelope"></span><span class="text"><span class="__cf_email__" data-cfemail="ff96919990bf86908a8d9b90929e9691d19c9092">contacto@payapp.com</span></span></a></li>
                            <li><span class="icon icon-clock-o"></span><span class="text">Lunes &mdash; Sabado<br> 9:00am - 6:00pm</span></li>
                            <li><span><i class="fa fa-whatsapp fa-1x" aria-hidden="true" style="color: #fff;"></i></span> <span class="text" style="padding-left: 20%;"><a href="https://wa.me/5215539294202?text=Necesito%20ayuda">5539294202</a> </span> </li>
                        </ul>
                    </div>
                    {{--<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>--}}
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>
                    Copyright &copy;
                     All rights reserved
                </p>
            </div>
        </div>
    </div>
</footer>
