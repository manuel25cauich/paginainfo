<head>
    <title>PayApp | PayApp la nueva manera de hacer crecer tu negocio. Recargas Electrónicas, Pago de Servicios desde WhatsApp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('pagina/images/payapp.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <script src="https://use.fontawesome.com/9ddf764465.js"></script>
    <link rel="stylesheet" href="{{ asset('pagina/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/aos.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/jquery.timepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ asset('pagina/css/style.css')}}">
</head>
