<script src="{{ asset('pagina/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery-migrate-3.0.1.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery.easing.1.3.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery.waypoints.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery.stellar.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/aos.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/jquery.animateNumber.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/scrollax.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/google-map.js')}}" type="text/javascript"></script>
<script src="{{ asset('pagina/js/main.js')}}" type="text/javascript"></script>
@stack('script')
