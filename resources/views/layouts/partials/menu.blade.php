<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('inicio')}}"><img src="{{ asset('pagina/images/payapp.png')}}" alt="" style="width: auto;height: 65px;"> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="{{ route('inicio')}}" class="nav-link">Inicio</a></li>
                <li class="nav-item"><a href="{{ route('plataforma')}}" class="nav-link">Nuestros Servicios</a></li>
                <li class="nav-item"><a href="{{ route('beneficios')}}" class="nav-link">Beneficios</a></li>
                <li class="nav-item"><a href="{{ route('proceso')}}" class="nav-link">Conoce el Proceso de Venta</a></li>
                {{--<li class="nav-item"><a href="{{ route('videos')}}" class="nav-link">Videos</a></li>
                <li class="nav-item"><a href="{{ route('recargas')}}" class="nav-link">Recargas</a> </li>--}}
                <li class="nav-item"><a href="{{ route('contacto')}}" class="nav-link">Contáctanos</a></li>
                <li class="nav-item"><a href="https://payapp.mx/payappv2/" class="nav-link">Acceso a Clientes</a></li>
            </ul>
        </div>
    </div>
</nav>
