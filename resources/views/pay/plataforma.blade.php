@extends('pay')
@section('section2')
  <section class="ftco-section">
    <div class="container">

      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
          <h2 class="mb-4">Vende cuando sea donde sea</h2>
          <p>Con nuestra plataforma solo requieres un dispositivo con Whatsapp para comenzar a ganar mas dinero, compatible con todas las versiones</p>
        </div>
      </div>

      <div class="row">

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{asset('pagina/images/tiempo-aire.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Venta de Tiempo Aire</h6>
            <p style="text-align: justify;">
              Te ofrecemos con un solo Saldo, Recargas de Tiempo Aire, <strong style="color: green;">para todas
              las Compañías</strong> (Telcel, Movistar, AT&T, UNEFON, Virgin, Cierto y más.
            </p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{asset('pagina/images/planes.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Venta de Planes de Datos</h6>
            <p style="text-align: justify;">
              <strong style="color: green;">Los Smartphones</strong> tiene funcionalidades que demandan <strong style="color: green;">Recargas  de
              productos de datos</strong> que ofrecen llamadas, mensajes, redes sociales
              además de megas para navegar por Internet. <strong style="color: green;">¡No pierdas ventas ofrece
              PAQUETES DE DATOS!. Contamos con Amigo sin Limite de Telcel, AT&T Plus,
              UNEFON Plus y Movistar Internet</strong>
            </p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{asset('pagina/images/servicios.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Recibir Pago de Servicios</h6>
            <p style="text-align: justify;">
              Ofrece a tus clientes el servicio de recibir el pago de sus servicios.
              Contamos con todos los servicios Nacionales, Recargas de Peaje para
              Autopistas, Pagos de Ventas de Catálogo y muchos más.
            </p>
          </div>
        </div>


        {{--<div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/recargas.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">RECARGAS ELECTRONICAS</h6>
            <p>Telcel, Movistar, Unefon, Nextel, Iusacell, At&t y Virgin Mobile</p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/televia.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">IAVE y TELEVÍA</h6>
            <p>Abona saldo a tus dispositios de peaje</p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/itunes.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">ITUNES Y APP STORE </h6>
            <p>Abona saldo con tarjetas prepago</p>
          </div>
        </div>--}}

      </div>

      <div class="row">

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{asset('pagina/images/entretenimiento.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Pines de Entretenimiento</h6>
            <p style="text-align: justify;">
              Ofrecer a tus clientes pines electrónicos, SIN TARJETAS FISICAS.
              El pin le llegará al cliente por un mensaje de texto a su celular,
              ADIOS TICKET. <br><br>
              Hay pines para ir a Cinepolis, rentar películas, comprar o rentar
              juegos y más.
            </p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{asset('pagina/images/pay.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Tarjetas de Prepago Paysafecard</h6>
            <p style="text-align: justify;">
              Tus clientes al adquirir un pin PAYSAFECARD, lo podrán utilizar en
              sitios web como medio de pago tales como: <strong style="color: green;">Spotify, Caliente, BetCris,
              PokerStar y más.</strong> Pago en efectivo y de UNA MANERA SEGURA, sin utilizar
              su tarjeta de crédito o débito.
            </p>
          </div>
        </div>

        {{--<div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/google.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">GOOGLE PLAY</h6>
            <p>Abona saldo con tarjetas prepago</p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/netflix.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">NETFLIX</h6>
            <p>Adquiere tu tarjeta prepago</p>
          </div>
        </div>

        <div class="col-md-4 mb-5 ftco-animate">
          <div class="block-10">
            <img src="{{ asset('pagina/images/cinepolis.png')}}" alt="" class="img-fluid mb-3">
            <h6 class="mb-4 text-center">Cinepolis</h6>
            <p>Compra tus boletos</p>
          </div>
        </div>--}}

      </div>
    </div>
  </section>

@endsection
