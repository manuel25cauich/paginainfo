@extends('pay')
@section('section1')
  <div class="hero-wrap">
    <div class="overlay"></div>
    <div class="circle-bg"></div>
    <div class="circle-bg-2"></div>
    <div class="container-fluid">
        <div class="row no-gutters d-flex slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-6 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
                <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="{{ route('inicio')}}">Inicio</a></span> <span>Videos</span></p>
                <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Videos</h1>
            </div>
        </div>
    </div>
  </div>
@endsection
@section('section2')
  <section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <h2 class="mb-4">Recargar</h2>
                <p>
                  <video src="{{ asset('pagina/images/recargar.mp4')}}" controls width="640" height="480"></video>
                </p>
                <h2 class="mb-4 mt-5">Reportar Deposito</h2>
                <p>
                  <video src="{{ asset('pagina/images/deposito.mp4')}}" controls width="640" height="480"></video>
                </p>
            </div>
        </div>
    </div>
</section>

@endsection
