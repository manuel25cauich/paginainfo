@extends('pay')
@section('section2')
  <section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">
      <h3 class="text-center">AFÍLIATE A NUESTRO INCREÍBLE PLATAFORMA, Y HAZ CRECER TU NEGOCIO.</h3><br>
        <div class="row block-9">
            <div class="col-md-6 pr-md-5">
              <form class="" action="{{ route('regisForm')}}" method="post">
                @csrf
                <label for="">Nombre</label>
                <input class="form-control" type="text" name="nombre" value="" required>
                <label for="">Apellido Paterno</label>
                <input class="form-control" type="text" name="apPaterno" value="" required>
                <label for="">Apellido Materno</label>
                <input class="form-control" type="text" name="apMaterno" value="" required>
                <label for="">Celular</label>
                <input class="form-control" type="text" name="telefono" value="" required maxlength="10">
                <label for="">Correo Electronico</label>
                <input class="form-control" type="email" name="correo" value="" required><br>
                <button class="btn btn-primary py-3 px-5" type="submit" name="button">Enviar</button>
              </form>
            </div>
            <div class="col-md-6" id="map"></div>
        </div>
    </div>
</section>
@endsection
