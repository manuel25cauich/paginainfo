@extends('pay')
@section('section1')
  <div class="text-center">
    <img class="img-fluid" src="{{ asset('pagina/images/banner55.png')}}" alt="">

  </div>
  {{--<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('pagina/images/1.png')}}" style="height: 756px;" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('pagina/images/2.png')}}" style="height: 756px;" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('pagina/images/3.png')}}" style="height: 756px;" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('pagina/images/4.png')}}" style="height: 756px;" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('pagina/images/5.png')}}" style="height: 756px;" alt="Third slide">
    </div>
    <a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    {{--<div class="main-text hidden-xs">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: center;">
          <h1 class="ih">Gana <br> Dinero <br> Whatsapeando.</h1>
          <p class="mb-md-5 mb-sm-3 iho">Haz Recargas vía:<br>
            <i class="fa fa-whatsapp" style="color: #fff"></i> WhatsApp <br>
            <i class="fa fa-telegram" style="color: #fff"></i> Telegram <br>
            <i class="fa fa-commenting" style="color: #fff"></i> SMS
          </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: center; background-color: #80808094; border-radius: 20px 20px;">
          <h1 class="ih">Registrate</h1>
          <form class="iho formu" action="{{ route('regisForm')}}" method="post">
            @csrf
            <div class="row">
              <div class="col-sm-4">
                <label for="">Nombre</label>
                <input class="form-control" type="text" name="nombre" value="" required>
              </div>
              <div class="col-sm-4">
                <label for="">Apellido Paterno</label>
                <input class="form-control" type="text" name="apPaterno" value="" required>
              </div>
              <div class="col-sm-4">
                <label for="">Apellido Materno</label>
                <input class="form-control" type="text" name="apMaterno" value="" required>
              </div>
            </div>
            <label for="">Celular</label>
            <input class="form-control" type="text" name="telefono" value="" required maxlength="10">
            <label for="">Correo Electronico</label>
            <input class="form-control" type="email" name="correo" value="" required>
            <button class="btn btn-primary py-3 px-5" type="submit" name="button">Enviar</button>
          </form>
        </div>
      </div>
    </div>--}}
  </div>
</div>
@endsection
@section('section2')
  {{--<div style="text-align: center; background-color: chartreuse">
    <p class="ih" style="color: black;">
      Recarga tiempo aire de todas las compañias <br>
       sin tramites fácil y rápido. <br>
      Te activamos en menos de 3 minutos.
    </p>
  </div>--}}
  @if (Session::has('success'))
    <div class="alert alert-success fade show text-center" role="alert">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      {{Session::get('success')}}
    </div>
  @endif
  <section class="ftco-section services-section">
      <div class="container">
          <div class="row justify-content-center mb-5 pb-5">
              <div class="col-md-7 text-center heading-section ftco-animate">
                  <h2 class="mb-4">PayApp la nueva manera de hacer crecer tu negocio.</h2>
              </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card">
		<video src="{{ asset('pagina/images/payappVideo.mp4')}}" autoplay controls height="410"></video>
                {{--<img class="card-img-top" src="{{ asset('pagina/images/baner3.jpg')}}" alt="Card image cap">--}}
                <div class="card-body">
                  <h5 class="card-title">Que es PayApp</h5>
                  <p class="card-text">PayApp es una plataforma de WhatsApp para la venta de tiempo aire y pago de servicios. Es 100% segura y confiable.</p>
                  <p class="card-text"><small class="text-muted"><a href="{{ route('plataforma')}}">Más Informacion</a></small></p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <img class="card-img-top" src="{{ asset('pagina/images/baner4.jpg')}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Comisiones</h5>
                  <p class="card-text">Incrementa tus ingresos de manera fácil, rápida y segura. Manejamos el mejor esquema de comisiones en el mercado. Descuentos en Recargas y Tarjetas de Regalo </p>
                  <p class="card-text"><small class="text-muted"><a href="{{ route('beneficios')}}">Más Informacion</a></small></p>
                </div>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <img class="card-img-top" src="{{ asset('pagina/images/baner2.jpg')}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Elige PayApp</h5>
                  <p class="card-text">Solo necesitas un dispositivo con WhatsApp para comenzar. Ofrecemos una amplia gama de productos y servicios.</p>
                  <p class="card-text"><small class="text-muted"><a href="{{ route('proceso')}}">Más Informacion</a></small></p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card">
                <img class="card-img-top" src="{{ asset('pagina/images/baner5.jpg')}}" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plataforma</h5>
                  <p class="card-text">El sistema te guía paso a paso en cualquier movimiento que realices: haz una recarga o abona saldo a tu cuenta. ¡Es muy fácil!</p>
                  <p class="card-text"><small class="text-muted"><a href="https://payapp.mx/payapp/">Ingresa</a></small></p>
                </div>
              </div>
            </div>

              {{--<div class="col-md-3 d-flex align-self-stretch ftco-animate">
                  <div class="media block-6 services d-block text-center" style="height: 220px !important;">
                      <div class="d-flex justify-content-center">
                            <div class="icon"><i class="fa fa-question fa-4x"></i> </div>
                      </div>
                      <div class="media-body p-2 mt-3">
                          <h3 class="heading">Que es PayApp</h3>
                          <p>PayApp es una plataforma de WhatsApp para la venta de tiempo aire y pago de servicios. Es 100% segura y confiable.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                  <div class="media block-6 services d-block text-center" style="height: 300px !important;">
                      <div class="d-flex justify-content-center">
                          <div class="icon"><i class="fa fa-money fa-4x" aria-hidden="true"></i></div>
                      </div>
                      <div class="media-body p-2 mt-3">
                          <h3 class="heading">Beneficios</h3>
                          <ul>
                            <li>Incrementa tus ingresos de manera fácil, rápida y segura.</li>
                            <li>Manejamos el mejor esquema de comisiones del mercado.</li>
                            <li>No necesitas hacer ninguna inversión inicial, no necesitas comprar saldo por adelantado.</li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                  <div class="media block-6 services d-block text-center" style="height: 220px !important;">
                      <div class="d-flex justify-content-center">
                          <div class="icon"><i class="fa fa-users fa-4x" aria-hidden="true"></i></div>
                      </div>
                      <div class="media-body p-2 mt-3">
                          <h3 class="heading">Elige PayApp</h3>
                          <ul>
                            <li>Solo necesitas un dispositivo con WhatsApp para comenzar.</li>
                            <li>Ofrecemos una amplia gama de productos y servicios.</li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                  <div class="media block-6 services d-block text-center" style="height: 220px !important;">
                      <div class="d-flex justify-content-center">
                          <div class="icon"><i class="fa fa-comment fa-4x" aria-hidden="true"></i></div>
                      </div>
                      <div class="media-body p-2 mt-3">
                          <h3 class="heading">Plataforma</h3>
                          <p>Nuestro sistema es fácil e inteligente.<br>El sistema te guía paso a paso en cualquier movimiento que realices: haz una recarga o abona saldo a tu cuenta. ¡Es muy fácil!</p>
                      </div>
                  </div>
              </div>--}}
      </div>
  </section><br><br>

@endsection
@push('script')
  <script type="text/javascript">
    $(document).ready(function(){
      var msg = '{{Session::get('success')}}';
      var exist = '{{Session::has('success')}}';
      console.log(msg);
      $("#text").text(msg);

      if (exist) {
        $("#exampleModal").modal('show');
      }
    });
  </script>
@endpush
