@extends('pay')
@section('section2')
  <section class="ftco-section bg-light">
      <div class="container">
          <div class="row">
              <div class="col-md-4 ftco-animate">
                  <div class="blog-entry">
                      <i class="fa fa-money bene"></i>
                      <div class="text p-4 d-block">
                          <div class="meta mb-3">
                              <h6 class="heading text-center">Duplica tus Ventas</h6>
                          </div>
                          <p class="text-center">RECUERDA CON ESTE SERVICIO MAS GENTE VISITARA TU NEGOCIO POR LO QUE ES UNA EXCELENTE OPCIÓN PARA INCREMENTAR TU GANANCIAS.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 ftco-animate">
                  <div class="blog-entry" data-aos-delay="100">
                      <i class="fa fa-users bene"></i>
                      <div class="text p-4">
                          <div class="meta mb-3">
                              <h6 class="heading text-center">Afiliate sin tramites</h6>
                          </div>
                          <p class="text-center">SIN CONTRATOS, SIN PLAZOS FORZOSOS O MÍNIMOS DE COMPRA, CON NOSOTROS SOLO ADQUIERES LO QUE NECESITAS CUANDO LO NECESITAS.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 ftco-animate">
                  <div class="blog-entry" data-aos-delay="200">
                      <i class="fa fa-handshake-o  bene"></i>
                      <div class="text p-4">
                          <div class="meta mb-3">
                            <h6 class="heading text-center">Fácil y sencillo</h6>
                          </div>
                          <p class="text-center">SOLO NECESITAS TENER WHATSAPP O PC PARA AFILIARTE CON NOSOTROS, ADEMAS CONTARAS CON ATENCIÓN Y SOPORTE EN TODO MOMENTO</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 ftco-animate">
                  <div class="blog-entry" data-aos-delay="100">
                      <i class="fa fa-certificate bene"></i>
                      <div class="text p-4">
                          <div class="meta mb-3">
                              <h6 class="heading text-center">Servicios Garantizado</h6>
                          </div>
                          <p class="text-center">CON NUESTRA PLATAFORMA REALIZAR RECARGAS SERÁ TAN FÁCIL COMO CONVERSAR CON UN AMIGO</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 ftco-animate">
                  <div class="blog-entry">
                      <i class="fa fa-usd bene"></i>
                      <div class="text p-4 d-block">
                          <div class="meta mb-3">
                              <h6 class="heading text-center">Mejores comisiones</h6>
                          </div>
                          <p class="text-center">EL MEJOR ESQUEMA DE COMISIONES EN EL MERCADO EN RECARGAS ELECTRONICAS Y GIFTCARDS DEL MERCADO.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-4 ftco-animate">
                <div class="blog-entry" data-aos-delay="100">
                  <i class="fa fa-shopping-bag bene"></i>
                  <div class="text p-4">
                    <div class="meta mb-3">
                      <h6 class="heading text-center">Tipos de Bolsa</h6>
                    </div>
                    <p class="text-center">PARA REALIZAR EL PAGO DE SERVICIOS REQUIERES DE UNA BOLSA DIFERENTE DE UNA BOLSA DIFERENTE DE TUS RECARGAS</p>
                  </div>

                </div>

              </div>

          </div>
      </div>
  </section>
@endsection
