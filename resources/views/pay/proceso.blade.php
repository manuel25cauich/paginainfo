@extends('pay')
@section('section2')
  <section class="ftco-section testimony-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
          <p>Todo lo que necesitas un dispositivo con WhatsApp para comenzar a utilizar el servicio no importa si es en tu Celular, Tablet o Computadora, con estos sencillas imágenes sabrás todo lo que necesitas.</p>
        </div>
      </div>
      <div class="row ftco-animate">
        <div class="col-md-12">
          <div class="carousel-testimony owl-carousel ftco-owl">

            <div class="item">
              <div class="testimony-wrap p-4 pb-5">
                <div class="text">
                  <img src="{{ asset('pagina/images/paso2-1.png')}}" alt="">
                  <p class="name">Paso 2:</p>
                  <span class="position">En pocos instantes recibirás un mensaje de WhatsApp dándote la bienvenida al servicio , recuerda guardar el numero en tus contactos para mejor referencia</span>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="testimony-wrap p-4 pb-5">
                <div class="text">
                  <img src="{{ asset('pagina/images/paso3-1.png')}}" alt="">
                  <p class="name">Paso 3:</p>
                  <span class="position">Abona saldo a tu cuenta de una manera fácil y rápida , solo envía la foto del depósito y quedara abonado en cuestión de minutos</span>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="testimony-wrap p-4 pb-5">
                <div class="text">
                  <img src="{{ asset('pagina/images/paso4-1.png')}}" alt="">
                  <p class="name">Paso 4:</p>
                  <span class="position">Comencemos a ganar dinero , siguiendo estos sencillos pasos podrás realizar cualquier transacción de una manera eficaz gracias a la innovadora plataforma que ofrecemos, realizar tus recargas y pagar tus servicios no podría ser de una manera más sencilla</span>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="testimony-wrap p-4 pb-5">
                <div class="text">
                  <img src="{{ asset('pagina/images/paso1-1.png')}}" alt="">
                  <p class="name">Paso 1:</p>
                  <span class="position"> Regístrate en la parte de abajo incluyendo nombre de usuario, contraseña y correo electrónico.</span>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ftco-animate">
              <h2 class="mb-4">Recargar</h2>
              <video src="{{ asset('pagina/images/recargar.mp4')}}" controls class="img-fluid"></video>
            </div>
            <div class="col-md-6 ftco-animate">
              <h2 class="mb-4">Reportar Deposito</h2>
              <video src="{{ asset('pagina/images/deposito.mp4')}}" controls class="img-fluid"></video>
            </div>
        </div>
    </div>
</section>

@endsection
