<!DOCTYPE html>
<html lang="en">

@section('header')
  @include('layouts.partials.header')
@show



<body>
  @section('menu')
    @include('layouts.partials.menu')
  @show

    <div class="">
          @yield('section1')
    </div>

    @yield('section2')


    @section('footer')
      @include('layouts.partials.footer')
    @show

    <div class="flotante">
    <p style="color: #000; font-weight: 700;">Registrate</p>
      <a class="miBoton" href="#" name="button" data-toggle="modal" data-target="#exampleModal">
        <i class="fa fa-user-plus fa-2x" aria-hidden="true" style="color: #fff"></i>
      </a>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Registro</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="" action="{{ route('regisForm')}}" method="post">
              @csrf
              <label for="">Nombre</label>
              <input class="form-control" type="text" name="nombre" value="" required>
              <label for="">Apellido Paterno</label>
              <input class="form-control" type="text" name="apPaterno" value="" required>
              <label for="">Apellido Materno</label>
              <input class="form-control" type="text" name="apMaterno" value="" required>
              <label for="">Celular</label>
              <input class="form-control" type="text" name="telefono" value="" required maxlength="10">
              <label for="">Correo Electronico</label>
              <input class="form-control" type="email" name="correo" value="" required><br>
              <button class="btn btn-primary py-3 px-5" type="submit" name="button">Enviar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    @section('scripts')
      @include('layouts.partials.script')
    @show


</body>

</html>
