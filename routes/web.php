<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('inicio');
});

Route::get('', 'PrincipalController@inicio')->name('inicio');
Route::get('plataforma', 'PrincipalController@plataforma')->name('plataforma');
Route::get('beneficios', 'PrincipalController@beneficios')->name('beneficios');
Route::get('proceso', 'PrincipalController@proceso')->name('proceso');
Route::get('videos', 'PrincipalController@videos')->name('videos');
Route::get('contacto', 'PrincipalController@contacto')->name('contacto');
Route::post('regis-form', 'PrincipalController@regisForm')->name('regisForm');
Route::any('/sendSms/{nombre}/{telefono}/{correo}', function($nombre, $telefono, $correo){

  include '../paginainfo/nusoap/nusoap.php';

  try {

    if (!isset($_SERVER['HTTP_REFERER'])) {
      return redirect('')->back();
    }

    $wsdl = "http://sms.tumundodeprepago.com/ws/status/soap.php?wsdl";

    $client = new nusoap_client($wsdl, 'wsdl');
    $err = $client->getError();
    if ($err) {
      echo '<h2>Constructor error</h2>'.$err;
      exit();
    }

    $mens = "Usuario a registrar en  PayApp. Nombre: ".$nombre." Telefono: ".$telefono." Correo: ".$correo;
    $parametros = array(
      'usuario' => '5556938963',
      'password' => '22222222',
      'dongle' => 'popocaca',
      'telefono' => '5539294202',
      'mensaje' => $mens
    );
    $result1 = $client->call('sendMsg', $parametros);

    //$mens1 = "Envia un mensaje a wa.me/5215613151512?texto=Hola y distruta de nuestro servicio";
    $mens1 = "Tus datos fueron enviados correctamente, en breve recibiras un sms con las instrucciones correspondientes.";
    $parametros1 = array(
      'usuario' => '5556938963',
      'password' => '22222222',
      'dongle' => 'popocaca',
      'telefono' => $telefono,
      'mensaje' => $mens1
    );

    $result1 = $client->call('sendMsg', $parametros1);

    if ($result1 == null) {
      return redirect()->route('inicio')
                       ->with('success', 'Tu registro fue exitoso, en un momento recibiras un mensaje de texto.');
    }else {
      return redirect()->route('inicio')
                       ->with('success', 'Tu registro fue exitoso, en un momento recibiras un mensaje de texto.');;
    }

  } catch (\Exception $e) {
    dd('No se envio mensaje');
  }




});
Route::get('privacite', 'PrincipalController@privacite')->name('privacite');
